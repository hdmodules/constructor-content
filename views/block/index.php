<?php
use hdmodules\content\models\Block;
use yii\helpers\Url;

\yii\bootstrap\BootstrapPluginAsset::register($this);

$this->title = 'Bloks';

$baseUrl = '/content';
?>

<?= $this->render('_menu') ?>

<?php if(sizeof($cats) > 0) : ?>
    <table class="table table-hover">
        <tbody>
            <?php foreach($cats as $cat) : ?>
                <tr>
                    <td width="50"><?= $cat->id ?></td>
                    <td style="padding-left:  <?= $cat->depth * 20 ?>px;">
                        <?php if(count($cat->children)) : ?>
                            <i class="caret"></i>
                        <?php endif; ?>
                        <?php if(!count($cat->children)) : ?>
                            <a href="<?= Url::to([ '/'. ($cat->controller_name ? $cat->controller_name : $baseUrl.'/item') , 'id' => $cat->id]) ?>" <?= ($cat->status == Block::STATUS_OFF ? 'class="table-hover"' : '') ?>><?= $cat->title ?></a>
                        <?php else : ?>
                            <span <?= ($cat->status == Block::STATUS_OFF ? 'class="table-hover' : '') ?>><?= $cat->title ?></span>
                        <?php endif; ?>
                    </td>

                    <td width="120" class="text-right" style="overflow: inherit;">
                        <div class="dropdown actions">
                            <i id="dropdownMenu<?= $cat->id ?>" data-toggle="dropdown" aria-expanded="true" title="<?= Yii::t('content', 'Actions') ?>" class="glyphicon glyphicon-menu-hamburger"></i>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu<?= $cat->id ?>">
                                <li><a href="<?= Url::to([$baseUrl.'/block/edit', 'id' => $cat->id]) ?>"><i class="glyphicon glyphicon-pencil font-12"></i> <?= Yii::t('content', 'Edit') ?></a></li>
                                <li><a href="<?= Url::to([$baseUrl.'/block/create', 'parent' => $cat->id]) ?>"><i class="glyphicon glyphicon-plus font-12"></i> <?= Yii::t('content', 'Add sub block') ?></a></li>
                                <li role="presentation" class="divider"></li>
                                <li><a href="<?= Url::to([$baseUrl.'/block/up', 'id' => $cat->id]) ?>"><i class="glyphicon glyphicon-arrow-up font-12"></i> <?= Yii::t('content', 'Move up') ?></a></li>
                                <li><a href="<?= Url::to([$baseUrl.'/block/down', 'id' => $cat->id]) ?>"><i class="glyphicon glyphicon-arrow-down font-12"></i> <?= Yii::t('content', 'Move down') ?></a></li>
                                <li role="presentation" class="divider"></li>
                                <?php if($cat->status == Block::STATUS_ON) :?>
                                    <li><a href="<?= Url::to([$baseUrl.'/block/off', 'id' => $cat->id]) ?>" title="<?= Yii::t('content', 'Turn Off') ?>'"><i class="glyphicon glyphicon-eye-close font-12"></i> <?= Yii::t('content', 'Turn Off') ?></a></li>
                                <?php else : ?>
                                    <li><a href="<?= Url::to([$baseUrl.'/block/on', 'id' => $cat->id]) ?>" title="<?= Yii::t('content', 'Turn On') ?>"><i class="glyphicon glyphicon-eye-open font-12"></i> <?= Yii::t('content', 'Turn On') ?></a></li>
                                <?php endif; ?>
                                <li><a href="<?= Url::to([$baseUrl.'/block/delete', 'id' => $cat->id]) ?>" class="confirm-delete" data-reload="1" title="<?= Yii::t('content', 'Delete block') ?>"><i class="glyphicon glyphicon-remove font-12"></i> <?= Yii::t('content', 'Delete') ?></a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php else : ?>
    <p><?= Yii::t('content', 'No records found') ?></p>
<?php endif; ?>
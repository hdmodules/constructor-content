<?php
$this->title = Yii::t('easyii', 'Create block');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model, 'parent' => $parent]) ?>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use hdmodules\base\widgets\SeoForm;
use hdmodules\base\helpers\Image;
use hdmodules\content\models\Block;
use hdmodules\base\multilanguage\input_widget\MultiLanguageActiveField;
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            
            <div class="x_title">
                <h2><?= Yii::t('content', 'Form') ?> <small><?= Yii::t('content', 'create block') ?></small></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            
            <div class="x_content">
                
                <?php $form = ActiveForm::begin([
                    'class'=>'form-horizontal form-label-left',
                    'enableAjaxValidation' => true,
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>

                <?= $form->field($model, 'title')->widget(MultiLanguageActiveField::className()) ?>

                <?php if(!empty($parent)) : ?>
                    <div class="form-group field-category-title required">
                        <label for="category-parent" class="control-label"><?= Yii::t('content', 'Parent block') ?></label>
                        <select class="form-control" id="category-parent" name="parent">
                            <option value="" class="smooth"><?= Yii::t('easyii', 'No') ?></option>
                            <?php foreach(Block::find()->sort()->asArray()->all() as $node) : ?>
                                <option value="<?= $node['id'] ?>" <?= ($parent == $node['id']) ? 'SELECTED' : '' ?> style="padding-left: <?= $node['depth']*20 ?>px;"><?= $node['title'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php endif; ?>

                <?php if($model->image) : ?>
                    <img src="<?= Image::thumb($model->image, 240) ?>">
                    <a href="<?= Url::to(['/content/block/clear-image', 'id' => $model->primaryKey]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear image')?>"><?= Yii::t('easyii', 'Clear image')?></a>
                <?php endif; ?>

                <?= $form->field($model, 'image')->fileInput() ?>

                <?= $form->field($model, 'slug') ?>

                <?= $form->field($model, 'controller_name') ?>

                <?= SeoForm::widget(['model' => $model]) ?>

                <div class="ln_solid"></div>
                <?= Html::submitButton(Yii::t('content', 'Save'), ['class' => 'btn btn-primary']) ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>


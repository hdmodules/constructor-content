<?php
use hdmodules\content\models\Item;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = Yii::t('content', 'Items');

$module = $this->context->module->id;
?>
<?= $this->render('_menu', ['block' => $model]) ?>

<div class="col-md-12 col-xs-12">
    <div class="x_panel">

        <div class="x_title"  style="margin-bottom: 30px">
            <h2><?= Yii::t('game', 'Games') ?></h2>
            <ul class="nav navbar-right">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">

            <?= GridView::widget([
                'dataProvider' =>$provider,
                'summary'=>'',
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['width' => '3%'],
                    ],
                    [
                        'attribute' => 'title',
                    ],
                    [
                        'attribute'=>'status',
                        'contentOptions' => ['width' => '90px'],
                        'content'=>function($data){
                            return \kartik\widgets\SwitchInput::widget([
                                'name' => 'status',
                                'options'=>[
                                    'class'=>'switch-input',
                                    'data-id' => $data->primaryKey,
                                    'data-link' => Url::to(['/content/item']),
                                ],
                                'value' => $data->status == \backend\models\GameItemContent::STATUS_ON,
                                'pluginOptions' => [
                                    'size' => 'small',
                                    'onColor' => 'success',
                                    'offColor' => 'danger',
                                ],
                            ]);
                        }
                    ],
                    [
                        'attribute' => 'views',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width'=>'85px'],
                        'template'=>'<div class="btn-group btn-group-sm">{up}{down}</div>',
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $url = '';
                            if ($action === 'up')
                                $url = Url::to(['/content/item/up', 'id'=>$model->id, 'block_id' => $model->block->primaryKey]);
                            if ($action === 'down')
                                $url = Url::to(['/content/item/down', 'id'=>$model->id, 'block_id' => $model->block->primaryKey]);

                            return $url;
                        },
                        'buttons' => [
                            'up' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', $url, [
                                    'title' => Yii::t('hdmodules', 'Move up'),
                                    'class'=>'btn btn-default move-up'
                                ]);
                            },
                            'down' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', $url, [
                                    'title' => Yii::t('hdmodules', 'Move down'),
                                    'class'=>'btn btn-default move-down'
                                ]);
                            },
                        ],

                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width'=>'85px'],
                        'template'=>'<div class="btn-group btn-group-sm">{update}{delete}</div>',
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $url = '';
                            if ($action === 'update')
                                $url = Url::to(['/content/item/edit', 'id'=>$model->id]);
                            if ($action === 'delete')
                                $url = Url::to(['/content/item/delete', 'id'=>$model->id]);

                            return $url;
                        },
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('hdmodules', 'Update'),
                                    'class'=>'btn btn-default update'
                                ]);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('hdmodules', 'Delete'),
                                    'class'=>'btn btn-danger delete',
                                    'data-confirm' => Yii::t('app', 'Are you sure you want to delete this Record?'),
                                    'data-method' => 'post'
                                ]);
                            },
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</div>

<?php  $this->registerJs('

$(\'.switch-input\').on(\'switchChange.bootstrapSwitch\', function(event, state){
        var checkbox = $(this);             
        $.getJSON(checkbox.data(\'link\') + \'/\' + (state ? \'on\' : \'off\') + \'/\' + checkbox.data(\'id\'), function(response){                    
            if(response.result === \'error\' || response.result == \'\'){                            
                checkbox.bootstrapSwitch(\'disabled\',true);
                alert(response.error);
            }        
        }).error(function(error) {         
            checkbox.bootstrapSwitch(\'disabled\',true);
            alert(error.statusText);
        });
    });
', \yii\web\View::POS_READY); ?>






<?php
namespace hdmodules\content\controllers;

use Yii;
use hdmodules\content\models\Block;
use hdmodules\base\controllers\Controller;
use hdmodules\base\helpers\Image;
use hdmodules\base\behaviors\SortableModel;
use yii\db\ActiveRecord as AR;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\UploadedFile;


class BlockController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index', [
            'cats' => Block::cats()
        ]);
    }

    public function actionCreate($parent = null)
    {
        $model = new Block();

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if(isset($_FILES)){
                    $model->image = UploadedFile::getInstance($model, 'image');
                    if($model->image && $model->validate(['image'])){
                        $model->image = Image::upload($model->image, 'block');
                    } else {
                        $model->image = '';
                    }
                }

                $model->status = Block::STATUS_ON;

                $parent = (int)Yii::$app->request->post('parent', null);
                if($parent > 0 && ($parentCategory = Block::findOne($parent))){
                    $model->order_num = $parentCategory->order_num;
                    $model->appendTo($parentCategory);
                } else {
                    $model->attachBehavior('sortable', SortableModel::className());
                    $model->makeRoot();
                }

                if(!$model->hasErrors()){
                    $this->flash('success', Yii::t('content', 'Block created'));
                    return $this->redirect(['/content/block/index']);
                }
                else{
                    $this->flash('error', Yii::t('content', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        }
        else {
            return $this->render('create', [
                'model' => $model,
                'parent' => $parent
            ]);
        }
    }

    public function actionEdit($id)
    {
        if(!($model = Block::findOne($id))){
            return $this->redirect(['/block/index']);
        }

        $model->loadTranslations_custom();

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if(isset($_FILES)){
                    $model->image = UploadedFile::getInstance($model, 'image');
                    if($model->image && $model->validate(['image'])){
                        $model->image = Image::upload($model->image, 'block');
                    }else{
                        $model->image = $model->oldAttributes['image'];
                    }
                }
                if($model->save()){
                    $this->flash('success', Yii::t('content', 'Block updated'));
                }
                else{
                    $this->flash('error', Yii::t('content', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

    public function actionDelete($id)
    {
        if(($model = Block::findOne($id))){
            $children = $model->children()->all();
            $model->deleteWithChildren();
            foreach($children as $child) {
                $child->afterDelete();
            }
        } else {
            $this->error = Yii::t('content', 'Not found');
        }
        return $this->formatResponse(Yii::t('content', 'Block deleted'));
    }

    public function actionClearImage($id)
    {
        $model = Block::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('content', 'Not found'));
        }
        elseif($model->image){
            $model->image = '';
            if($model->update()){
                $this->flash('success', Yii::t('content', 'Image cleared'));
            } else {
                $this->flash('error', Yii::t('content', 'Update error. {0}', $model->formatErrors()));
            }
        }
        return $this->back();
    }

    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }
    
    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }
    
    public function actionOn($id)
    {
        return $this->changeStatus($id, Block::STATUS_ON);
    }
    
    public function actionOff($id)
    {
        return $this->changeStatus($id, Block::STATUS_OFF);
    }
    
    private function move($id, $direction)
    {
        if(($model = Block::findOne($id)))
        {
            $up = $direction == 'up';
            $orderDir = $up ? SORT_ASC : SORT_DESC;

            if($model->depth == 0){

                $swapCat = Block::find()->where([$up ? '>' : '<', 'order_num', $model->order_num])->orderBy(['order_num' => $orderDir])->one();
                if($swapCat)
                {
                    Block::updateAll(['order_num' => '-1'], ['order_num' => $swapCat->order_num]);
                    Block::updateAll(['order_num' => $swapCat->order_num], ['order_num' => $model->order_num]);
                    Block::updateAll(['order_num' => $model->order_num], ['order_num' => '-1']);
                    $model->trigger(AR::EVENT_AFTER_UPDATE);
                }
            } else {
                $where = [
                    'and',
                    ['tree' => $model->tree],
                    ['depth' => $model->depth],
                    [($up ? '<' : '>'), 'lft', $model->lft]
                ];

                $swapCat = Block::find()->where($where)->orderBy(['lft' => ($up ? SORT_DESC : SORT_ASC)])->one();
                if($swapCat)
                {
                    if($up) {
                        $model->insertBefore($swapCat);
                    } else {
                        $model->insertAfter($swapCat);
                    }

                    $swapCat->update();
                    $model->update();
                }
            }
        }
        else {
            $this->flash('error', Yii::t('content', 'Not found'));
        }
        return $this->back();
    }

    public function changeStatus($id, $status)
    {
        $ids = [];

        if(($model = Block::findOne($id))){
            $ids[] = $model->primaryKey;
            foreach($model->children()->all() as $child){
                $ids[] = $child->primaryKey;
            }
            Block::updateAll(['status' => $status], ['in', 'id', $ids]);
            $model->trigger(AR::EVENT_AFTER_UPDATE);
        }
        else{
            $this->error = Yii::t('content', 'Not found');
        }

        return $this->formatResponse(Yii::t('content', 'Status successfully changed'));
    }
}
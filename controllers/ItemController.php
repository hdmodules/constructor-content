<?php
namespace hdmodules\content\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

use hdmodules\base\controllers\SortableController;
use hdmodules\base\controllers\StatusController;
use hdmodules\base\controllers\Controller;
use hdmodules\base\helpers\Image;
use hdmodules\content\models\Block;
use hdmodules\content\models\Item;

class ItemController extends Controller
{
    public $viewPath = '@vendor/hdmodules/constructor-content/views/item';

    public $viewPathIndex = '@vendor/hdmodules/constructor-content/views/item';

    public $viewPathPhotos = '@vendor/hdmodules/constructor-content/views/item';

    public $class_model = 'hdmodules\\content\\models\\Item';

    public function behaviors()
    {
        return array_merge( parent::behaviors(),
            [
                'sortable'=>[
                    'class' => SortableController::className(),
                    'model' => Item::className(),
                ],
                [
                    'class' => StatusController::className(),
                    'model' => Item::className()
                ]
            ]
        );
    }

    public function actionIndex($id)
    {
        if(!($model = Block::findOne($id))){
            return $this->redirect(['/item/index']);
        }

        $provider = new \yii\data\ActiveDataProvider([
            'query' => Item::find()->where(['block_id' => $model->id])->orderBy(['order_num' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render($this->viewPathIndex.'/index', [
            'model' => $model,
            'provider'=>$provider
        ]);
    }


    public function actionCreate($id)
    {
        if(!($block = Block::findOne($id))){
            return $this->redirect(['/content/block/index']);
        }

        $model = new $this->class_model();

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else {
                $model->block_id = $block->primaryKey;

                /*if (isset($_FILES)) {
                    $model->image = UploadedFile::getInstance($model, 'image');
                    if ($model->image && $model->validate(['image'])) {
                        $model->image = Image::upload($model->image, 'item');
                    } else {
                        $model->image = '';
                    }
                }*/

                if ($model->save()) {
                    $this->flash('success', Yii::t('content', 'Item created'));
                    return $this->redirect([($block->controller_name ? $block->controller_name.'/edit' :'/content/item/edit'), 'id' => $model->primaryKey]);
                } else {
                    $this->flash('error', Yii::t('content', 'Create error. {0}', $model->formatErrors()));
                    //return $this->refresh();
                }
            }
        }
        //else {
            return $this->render($this->viewPath.'/create', [
                'model' => $model,
                'block' => $block,
            ]);
        //}
    }

    public function actionEdit($id)
    {
        $class_model = $this->class_model;

        if(!($model = $class_model::findOne($id))){
            return $this->redirect(['/block/index']);
        }

        $model->loadTranslations_custom();

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else {
                /*if (isset($_FILES)) {
                    $model->image = UploadedFile::getInstance($model, 'image');
                    if ($model->image && $model->validate(['image'])) {
                        $model->image = Image::upload($model->image, 'item');
                    } else {
                        $model->image = $model->oldAttributes['image'];
                    }
                }*/

                if ($model->save()) {
                    $this->flash('success', Yii::t('content', 'Item updated'));
                    return $this->redirect([($model->block->controller_name ? $model->block->controller_name.'/edit' :'/content/item/edit'), 'id' => $model->primaryKey]);
                } else {
                    $this->flash('error', Yii::t('content', 'Update error. {0}', $model->formatErrors()));
                    //return $this->refresh();
                }
            }
        }
        //else {
            return $this->render($this->viewPath.'/edit', [
                'model' => $model,
            ]);
        //}
    }

    public function actionPhotos($id)
    {
        $class_model = $this->class_model;

        if(!($model = $class_model::findOne($id))){
            return $this->redirect(['/block/index']);
        }

        return $this->render($this->viewPathPhotos.'/photos', [
            'model' => $model,
        ]);
    }

    public function actionClearImage($id)
    {
        $class_model = $this->class_model;

        $model = $class_model::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('content', 'Not found'));
        }
        elseif($model->image){
            $model->image = '';
            if($model->save(false)){
                $this->flash('success', Yii::t('content', 'Image cleared'));
            } else {
                $this->flash('error', Yii::t('content', 'Update error. {0}', $model->formatErrors()));
            }
        }
        return $this->back();
    }

    public function actionDelete($id)
    {
        $class_model = $this->class_model;

        if(($model = $class_model::findOne($id))){
            $model->delete();
        } else {
            $this->error = Yii::t('content', 'Not found');
        }
        return $this->formatResponse(Yii::t('content', 'Item deleted'));
    }

    public function actionUp($id, $block_id)
    {
        return $this->move($id, 'up', ['block_id' => $block_id]);
    }

    public function actionDown($id, $block_id)
    {
        return $this->move($id, 'down', ['block_id' => $block_id]);
    }

    public function actionOn($id)
    {
        return $this->changeStatus($id, Item::STATUS_ON);
    }

    public function actionOff($id)
    {
        return $this->changeStatus($id, Item::STATUS_OFF);
    }
}
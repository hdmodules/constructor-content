<?php

use yii\db\Migration;
use yii\db\Schema;

class m000000_000000_content_blocks extends Migration
{

    public function up()
    {
        $this->createTable('content_block', [
            'id' => Schema::TYPE_PK,
            'title' => $this->string(255),
            'image' => $this->string(255),
            'short' => Schema::TYPE_TEXT,
            'text' => Schema::TYPE_TEXT,
            'text_1' => Schema::TYPE_TEXT,
            'text_2' => Schema::TYPE_TEXT,
            'order_num' => Schema::TYPE_INTEGER,
            'slug' => $this->string(128),
            'tree' => Schema::TYPE_INTEGER,
            'lft' => Schema::TYPE_INTEGER,
            'rgt' => Schema::TYPE_INTEGER,
            'depth' => Schema::TYPE_INTEGER,
            'create_time'=> Schema::TYPE_TIMESTAMP . ' NULL',
            'update_time'=> Schema::TYPE_TIMESTAMP . ' on update CURRENT_TIMESTAMP NOT NULL',
            'status' => $this->integer(1)->defaultValue(1),
            'time' => $this->integer(11)->defaultValue(0),
            'controller_name' => $this->string(255),
        ],  'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1' );

        $this->createTable('content_blocks_item', [
            'id' => Schema::TYPE_PK,
            'block_id' => $this->integer()->notNull(),
            'title' => $this->string(255),
            'image' => $this->string(255),
            'short' => Schema::TYPE_TEXT,
            'text' => Schema::TYPE_TEXT,
            'text_1' => Schema::TYPE_TEXT,
            'text_2' => Schema::TYPE_TEXT,
            'order_num' => Schema::TYPE_INTEGER,
            'slug' => $this->string(128),
            'create_time'=> Schema::TYPE_TIMESTAMP . ' NULL',
            'update_time'=> Schema::TYPE_TIMESTAMP . ' on update CURRENT_TIMESTAMP NOT NULL',
            'views' => $this->integer(11)->defaultValue(0),
            'status' => $this->integer(1)->defaultValue(1),
            'time' => $this->integer(11)->defaultValue(0),
        ],  'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1' );
        
        $this->createIndex('block_id', 'content_blocks_item', 'block_id');
        $this->addForeignKey('fk1_landing_block', 'content_blocks_item', 'block_id', 'content_block', 'id', 'CASCADE');

    }

    public function down()
    {   
        $this->dropTable('content_block');
        $this->dropTable('content_blocks_item');
        
        echo "m000000_000000_content_blocks was deleted.\n";
         
        return true;
    }

}

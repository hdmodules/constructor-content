<?php
namespace hdmodules\content\models;

use yii\db\ActiveQuery;
use hdmodules\base\models\Publish;

class ItemQuery extends ActiveQuery
{
    public function block($slug)
    {
        return $this->joinWith(['block'])->where(['`content_block`.slug' => $slug]);
    }

    public function status($status)
    {
        return $this->andWhere(['`content_block`.status' => $status]);
    }
            /**
     * Order by primary key ASC
     * @return $this
     */
    public function publish()
    {
        $model = new $this->modelClass;
        $this->leftJoin('`publish` p', '`content_blocks_item`.id = p.model_id and p.lang = :lang and table_name = :table', ['lang' => \Yii::$app->language, 'table' => $model->tableName()]);
        $this->andWhere('p.status = :status', ['status' => Publish::PUBLISHED]);
        return $this;
    }
}

<?php
namespace hdmodules\content\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use hdmodules\base\behaviors\SeoBehavior;
use hdmodules\base\models\Photo;
use hdmodules\base\behaviors\CacheFlush;
use hdmodules\base\behaviors\SortableModel;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

use hdmodules\base\multilanguage\MultiLanguageBehavior;
use hdmodules\base\multilanguage\MultiLanguageTrait;
use hdmodules\base\multilanguage\models\TranslationsWithString;
use hdmodules\base\behaviors\PublishBechavior;
use yii\web\UploadedFile;
use hdmodules\base\helpers\Image;

class Item extends \hdmodules\base\components\ActiveRecord
{
    use MultiLanguageTrait;

    const STATUS_OFF = 0;
    const STATUS_ON = 1;


    public static function tableName()
    {
        return 'content_blocks_item';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'short', 'text'], 'trim'],
            ['title', 'string', 'max' => 128],
            ['image', 'image'],
            [['block_id', 'views', 'status'], 'integer'],
            ['time', 'default', 'value' => time()],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('content', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['status', 'default', 'value' => self::STATUS_ON],
            [['title', 'short', 'text', 'text_1', 'text_2', 'attribute_1', 'attribute_2', 'attribute_3'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('content', 'Title'),
            'text' => Yii::t('content', 'Text'),
            'short' => Yii::t('content/article', 'Short'),
            'image' => Yii::t('content', 'Image'),
            'time' => Yii::t('content', 'Date'),
            'slug' => Yii::t('content', 'Slug'),
        ];
    }

    public function behaviors()
    {
        return [
            'sortable' => [
                'class' => SortableModel::className(),
                'par_id' => 'block_id'
            ],
            'cacheflush' => [
                'class' => CacheFlush::className(),
                'key' => [static::tableName().'_tree', static::tableName().'_flat']
            ],
            'seoBehavior' => SeoBehavior::className(),
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true
            ],
            'mlBehavior' => [
                'class' => MultiLanguageBehavior::className(),
                'mlConfig' => [
                    'db_table' => 'translations_with_string',
                    'attributes' => ['title', 'short', 'text', 'text_1', 'text_2', 'attribute_1', 'attribute_2', 'attribute_3'],
                    'admin_routes' => [
                        'content/item/*'
                    ],
                ],
            ],
            'publishBechavior' => [
                'class' => PublishBechavior::className(),
                'table_name' => self::tableName(),
            ],
        ];
    }

    public function getBlock()
    {
        return $this->hasOne(Block::className(), ['id' => 'block_id']);
    }

    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['item_id' => 'id'])->where(['class' => self::className()])->sort();
    }

    public function getTranslations()
    {
        return $this->hasMany(TranslationsWithString::className(), ['model_id' => 'id'])->andWhere(['table_name'=>'content_blocks_item', 'lang'=>Yii::$app->language]);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if (isset($_FILES) && !empty($_FILES)) {
                $this->image = UploadedFile::getInstance($this, 'image');
                if ($this->image && $this->validate(['image'])) {
                    $this->image = Image::upload($this->image, 'item');
                } else {
                    if($this->isNewRecord){
                        $this->image = '';
                    }else{
                        $this->image = $this->oldAttributes['image'];
                    }
                }
            }

            if(!$insert && $this->image != $this->oldAttributes['image'] && $this->oldAttributes['image']){
                @unlink(Yii::getAlias('@webroot').$this->oldAttributes['image']);
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();

        if($this->image){
            @unlink(Yii::getAlias('@webroot').$this->image);
        }

        if($this->photos){
            foreach($this->getPhotos()->all() as $photo){
                $photo->delete();
            }
        }
    }

    public static function listAll($slug = null, $keyField = 'id', $valueField = 'title', $asArray = true  )
    {
        if($block = Block::find()->where(['slug'=>$slug])->one()){
            $query = static::find()->where(['block_id'=>$block->id]);
        }else{
            return [];
        }

        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }

    public static function find()
    {
        return new ItemQuery(get_called_class());
    }

}
<?php
namespace hdmodules\content\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use hdmodules\base\behaviors\CacheFlush;
use hdmodules\base\behaviors\SeoBehavior;
use creocoder\nestedsets\NestedSetsBehavior;
use hdmodules\base\components\ActiveQueryNS;

use hdmodules\base\multilanguage\MultiLanguageBehavior;
use hdmodules\base\multilanguage\MultiLanguageTrait;
use yii\helpers\ArrayHelper;

class Block extends \hdmodules\base\components\ActiveRecord
{
    use MultiLanguageTrait;

    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    public static function tableName()
    {
        return 'content_block';
    }

    public function rules()
    {
        return [
            [['slug', 'title'], 'required'],
            ['title', 'trim'],
            ['title', 'string', 'max' => 128],
            ['controller_name', 'string', 'max' => 255],
            ['image', 'image'],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('content', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['slug', 'unique'],
            ['status', 'integer'],
            ['status', 'default', 'value' => self::STATUS_ON]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('content', 'Title'),
            'image' => Yii::t('content', 'Image'),
            'slug' => Yii::t('content', 'Slug'),
            'controller_name' => Yii::t('content', 'Controller name'),
        ];
    }

    public function behaviors()
    {
        return [
            'cacheflush' => [
                'class' => CacheFlush::className(),
                'key' => [static::tableName().'_tree', static::tableName().'_flat']
            ],
            'seoBehavior' => SeoBehavior::className(),
            /*'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true
            ],*/
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'tree'
            ],
            'mlBehavior' => [
                'class' => MultiLanguageBehavior::className(),
                'mlConfig' => [
                    'db_table' => 'translations_with_string',
                    'attributes' => ['title'],
                    'admin_routes' => [
                        'admin/*'
                    ],
                ],
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if(!$insert && $this->image != $this->oldAttributes['image'] && $this->oldAttributes['image']){
                @unlink(Yii::getAlias('@webroot').$this->oldAttributes['image']);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return ActiveQueryNS
     */
    public static function find()
    {
        return new ActiveQueryNS(get_called_class());
    }

    /**
     * Get cached tree structure of category objects
     * @return array
     */
    public static function tree()
    {
        $cache = Yii::$app->cache;
        $key = static::tableName().'_tree';

        $tree = $cache->get($key);
        if(!$tree){
            $tree = static::generateTree();
            $cache->set($key, $tree, 3600);
        }
        return $tree;
    }

    /**
     * Get cached flat array of category objects
     * @return array
     */
    public static function cats()
    {
        $cache = Yii::$app->cache;
        $key = static::tableName().'_flat';

        $flat = $cache->get($key);
        if(!$flat){
            $flat = static::generateFlat();
            $cache->set($key, $flat, 3600);
        }
        return $flat;
    }

    /**
     * Generates tree from categories
     * @return array
     */
    public static function generateTree()
    {
        $collection = static::find()->with('seo')->sort()->asArray()->all();
        $trees = array();
        $l = 0;

        if (count($collection) > 0) {
            // Node Stack. Used to help building the hierarchy
            $stack = array();

            foreach ($collection as $node) {
                $item = $node;
                unset($item['lft'], $item['rgt'], $item['order_num']);
                $item['children'] = array();

                // Number of stack items
                $l = count($stack);

                // Check if we're dealing with different levels
                while($l > 0 && $stack[$l - 1]->depth >= $item['depth']) {
                    array_pop($stack);
                    $l--;
                }

                // Stack is empty (we are inspecting the root)
                if ($l == 0) {
                    // Assigning the root node
                    $i = count($trees);
                    $trees[$i] = (object)$item;
                    $stack[] = & $trees[$i];

                } else {
                    // Add node to parent
                    $item['parent'] = $stack[$l - 1]->id;
                    $i = count($stack[$l - 1]->children);
                    $stack[$l - 1]->children[$i] = (object)$item;
                    $stack[] = & $stack[$l - 1]->children[$i];
                }
            }
        }

        return $trees;
    }

    /**
     * Generates flat array of categories
     * @return array
     */
    public static function generateFlat()
    {
        $collection = static::find()->with('seo')->sort()->asArray()->all();
        $flat = [];

        if (count($collection) > 0) {
            $depth = 0;
            $lastId = 0;
            foreach ($collection as $node) {
                $node = (object)$node;
                $id = $node->id;
                $node->parent = '';

                if($node->depth > $depth){
                    $node->parent = $flat[$lastId]->id;
                    $depth = $node->depth;
                } elseif($node->depth == 0){
                    $depth = 0;
                } else {
                    if ($node->depth == $depth) {
                        $node->parent = $flat[$lastId]->parent;
                    } else {
                        foreach($flat as $temp){
                            if($temp->depth == $node->depth){
                                $node->parent = $temp->parent;
                                $depth = $temp->depth;
                                break;
                            }
                        }
                    }
                }
                $lastId = $id;
                unset($node->lft, $node->rgt);
                $flat[$id] = $node;
            }
        }

        foreach($flat as &$node){
            $node->children = [];
            foreach($flat as $temp){
                if($temp->parent == $node->id){
                    $node->children[] = $temp->id;
                }
            }
        }

        return $flat;
    }

    public function getItems()
    {
        return $this->hasMany(Item::className(), ['block_id' => 'id'])->orderBy(['order_num' => SORT_DESC]);
    }
    
    public function getPublishItems()
    {
        return $this->hasMany(Item::className(), ['block_id' => 'id'])
                ->leftJoin('`publish`', '`publish`.model_id = `content_blocks_item`.id')
                ->andWhere(['`publish`.`status`' => 1, '`publish`.`table_name`' => Item::tableName(), '`publish`.`lang`' => Yii::$app->language])
                ->orderBy(['`content_blocks_item`.`order_num`' => SORT_DESC]);
    }
    
    public function afterDelete()
    {
        parent::afterDelete();

        foreach ($this->getItems()->all() as $item) {
            $item->delete();
        }
    }

    public static function listAll($slug, $keyField = 'id', $valueField = 'title', $asArray = true  )
    {
        if($block = Block::find()->where(['slug'=>$slug])->one()){
            $query = $block->children();
        }else{
            return [];
        }

        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }
}